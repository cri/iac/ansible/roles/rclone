# rclone

This role aims to deploy [rclone](https://rclone.org/) on a given host and to
setup multiples rclone jobs on a host. Those jobs will be scheduled using
cronjobs.

In this role, the approach taken was to make each **synchronization** job fully
configurable with the Ansible vars (yaml dictionaries). To handle multiple jobs
per host, you have to put each job's dictionary in a list named
`rclone_jobs_list`.

In its initial version this role supports any rclone command but only `sync`
can be achieved with clean wrapping.

Example of playbook:
```yaml
---

- hosts: all
  roles:
    - {role: rclone, tags: ['rclone']}
```

## Command examples for sync job administration
Manually synchronize to/from remote backend:
```bash
job_name.sh cri_sync
```

List files in remote backend:
```bash
job_name.sh rclone lsf backend:
```

## Architecture

### Scripts scopes:
#### retry_handler.sh
Handles retry and alerting, calls job_name.sh and handles its output.

#### job_name.sh
Set the given sync job configuration and call the wrapper. It can be used to
manually run rclone commands but no alerting will be generated.

#### cri_rclone_wrapper.sh
Define some functions for sync (for now). These functions are named `cri_*`.
This wrapper handles the configuration of the rclone backend from the
configuration given by `job_name.sh`. You can use the wrapper with other rclone
commands. This is very useful to access the rclone backend directly.

### Sync job pipeline:
cron -> retry_handler.sh -> job_name.sh -> cri_rclone_wrapper.sh -> rclone
                         -> cri_alerting.sh if alerting is enabled

## Alerting backends
You can enable and configure multiple alerting backends for each job.

Alerting configuration : (Tested([x]) or not([ ]))
- mail
- slack
- mattermost
- healthchecks.io
- telegram
- rocketchat
- discord
- node
- libnotify # for systemd desktops only

As the alerting logic is imported from `cri.restic` role, see [its
README](https://gitlab.cri.epita.fr/cri/iac/ansible/roles/restic/#alerting-backends)
for more information.

# Example conf for a host:
```yaml
rclone_jobs_list:
- name: my_default_sync
  enabled: true
  hostname: "my_hostname"           # (optional if a default var exists)
  alerting:                         # (optional if a default var exists)
    mail:
      enabled: true
      dest: "root@localhost"
    slack:
      enabled: true
      channel: "alerts-rclone"
      webhook_url: "https://hooks.slack.com/services/..."
  alerting_on_success: true         # (optional if a default var exists)
  alerting_on_success_canary: true  # (optional if a default var exists)
  alerting_on_success_canary_percent: "5" # (optional if a default var exists)
  max_attempts: "3"                 # (optional if a default var exists)
  retry_interval: "600"             # (optional if a default var exists)
  # By default, these commands refer to the commands defined in `rclone_wrapper.sh`
  job_cmd: "cri_sync"		    # (optional if a default var exists)
  extra_vars:                       # (optional if a default var exists or not needed)
    - { name: MYVAR, value: "A VALUE" }
  # Name of backend section in rclone configuration file
  backend_name: "backend"	    # (optional if a default var exists or not needed)
  backend:			    # Refer to rclone configuration documentation
    type: s3
    provider: Other
    acl: private
    access_key_id: ""
    secret_access_key: ""
    endpoint: "https://s3.example.org/"
  # Can be used in your templated hooks
  hooks_settings: {}
  # Used in cri_* functions only.
  # Local (from configured host POV) rclone target path
  # This defaults to `<job directory>/data` and can be templated with
  # `rclone_job_name` as follows:
  local_path: "/data/{rclone_job_name}"
  # Used in cri_* functions only.
  # Determines order of local and remote targets:
  # * `local` will sync local path to remote ("backend") path
  # * `remote` will sync remote path to local one
  source: "local"
  # Simple yaml list of the hooks filenames
  rclone_additional_args:
    - "--ignore-existing"
  job_pre_hooks:                 # (default to an empty list)
    - cmd:
      - "echo"
      - "This is a prehook"
      type: ""                      # (default to an empty string)
      name: "echo-cmd"              # (optional, only used for deployment output)
    - cmd:
      - "my_awesome_script.sh"
      - "This is a prehook"
      type: "template"              # (can also be a file if no templating is needed)
  job_post_hooks: []		    # (default to an empty list)
  job_cron:
    enable: true                    # (optional if a default var exists)
    minute: '0'                     # (optional if a default var exists)
    hour: '3'                       # (optional if a default var exists)
    day: '*'                        # (optional if a default var exists)
    weekday: '*'                    # (optional if a default var exists)
    month: '*'                      # (optional if a default var exists)

- name: my_custom_rclone_command
  job_cmd: "rclone cp /tmp/my-file.txt backend:/mybucket/my-file.txt"
  backend:
    type: s3
    provider: Other
    acl: private
    access_key_id: ""
    secret_access_key: ""
    endpoint: "https://s3.example.org/"
```

# How to disable a job
It's really simple, don't change anything in the configuration of your job
except the setting `enabled`:
```yaml
rclone_jobs_list:
 - name: my_default_job
   enabled: false
```
You can now deploy before deleting the entry from your YAML configuration file.
The role will take care of everything.

# Acknowledgements

`rclone` role is largely inspired from [this restic
role](https://gitlab.cri.epita.fr/cri/iac/ansible/roles/restic/).
