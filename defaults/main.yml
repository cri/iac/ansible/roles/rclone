---

# defaults/main.yml

# Paths for rclone install
rclone_srv_path: "/srv"
rclone_path: "{{ rclone_srv_path }}/rclone"

rclone_hostname: "{{ inventory_hostname }}"

# Args to be passed to rclone binary
rclone_default_additional_args: []

rclone_backend_default_name: "backend"

# Whether sync source should be `local` or `remote`
rclone_default_source: "local"

# Path to sync data to/from
rclone_backend_default_path: ""

# Arch map
rclone_host_arch_map:
  aarch64: 'arm64'
  armv6l: 'arm'
  armv7l: 'arm'
  i386: '386'
  x86_64: 'amd64'

# Host arch
rclone_host_arch: "{{ rclone_host_arch_map[ansible_architecture] | default(ansible_architecture) }}"

# Type of host
# This will be used for some types of desktop alerting
rclone_desktop_host: false

# Rclone version
# valid values: every version available as a release on github,
# without leading "v"
# renovate: repository="rclone/rclone"
rclone_version: "1.59.2"

# Path for hooks outside of the role
# Can be anything, most likely a relative path like '../../my_shared_templates'
rclone_files_hooks_path: ""
rclone_templates_hooks_path: ""
# Next after the previous paths
rclone_hooks_path: ""

# Sensible defaults
## Alerting
rclone_alerting: "{{ rclone_alerting_mail_default
                        or rclone_alerting_slack_default
                        or rclone_alerting_mattermost_default
                        or rclone_alerting_telegram_default
                        or rclone_alerting_rocketchat_default
                        or rclone_alerting_discord_default
                        or rclone_alerting_node_default
                        or rclone_alerting_libnotify_default
                        }}"

# Enable or disabled alerts when job is successful
rclone_alerting_on_success: true
rclone_alerting_on_success_canary: false
rclone_alerting_on_success_canary_percent: "5"

rclone_alerting_default_body: "Rclone default body"
rclone_alerting_default_subject: "Rclone default subject"

# Slack max body length
rclone_alerting_body_max_length: "3001"

rclone_alerting_default_color_fail: "#f52f2f"
rclone_alerting_default_color_success: "#71c074"

rclone_alerting_default_curl_max_time: "10"
rclone_alerting_default_curl_retry: "5"

rclone_alerting_default_emoji_fail: '\U0010071'      # :exclamation: in Unicode
rclone_alerting_default_emoji_success: '\U0002705'   # :white_check_mark: in Unicode
rclone_alerting_default_emote_fail: ":exclamation:"
rclone_alerting_default_emote_success: ":white_check_mark:"

### Mail
rclone_alerting_mail_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_mail_default_subject: "{{ rclone_alerting_default_subject }}"
rclone_alerting_mail_from: "rclone@example.com"
rclone_alerting_mail_tag: "RCLONE"
### Command used to send the mails
### valid values: sendmail, mail
rclone_alerting_mail_command: "sendmail"
### Cronjob env var PATH doesn't include `/usr/sbin` by default
rclone_alerting_sendmail_path: "/usr/sbin/sendmail"

rclone_alerting_mail_default: false
### Comma separated list of recipients
rclone_alerting_mail_default_dest: "root@localhost"

### Slack
rclone_alerting_slack_color_fail: "{{ rclone_alerting_default_color_fail }}"
rclone_alerting_slack_color_success: "{{ rclone_alerting_default_color_success }}"
rclone_alerting_slack_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_slack_curl_retry: "{{ rclone_alerting_default_curl_retry }}"
rclone_alerting_slack_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_slack_default_title: "{{ rclone_alerting_default_subject }}"
rclone_alerting_slack_emoji_fail: "{{ rclone_alerting_default_emote_fail }}"
rclone_alerting_slack_emoji_success: "{{ rclone_alerting_default_emote_success }}"

rclone_alerting_slack_default: false
rclone_alerting_slack_default_channel: "alerts-rclone"
rclone_alerting_slack_default_webhook_url: ""

### Mattermost
rclone_alerting_mattermost_color_fail: "{{ rclone_alerting_default_color_fail }}"
rclone_alerting_mattermost_color_success: "{{ rclone_alerting_default_color_success }}"
rclone_alerting_mattermost_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_mattermost_curl_retry: "{{ rclone_alerting_default_curl_retry }}"
rclone_alerting_mattermost_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_mattermost_default_title: "{{ rclone_alerting_default_subject }}"
rclone_alerting_mattermost_emoji_fail: "{{ rclone_alerting_default_emote_fail }}"
rclone_alerting_mattermost_emoji_success: "{{ rclone_alerting_default_emote_success }}"

rclone_alerting_mattermost_default: false
rclone_alerting_mattermost_default_channel: "alerts-rclone"
rclone_alerting_mattermost_default_webhook_url: ""

### Healthchecks.io
rclone_alerting_healthchecks_io_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_healthchecks_io_curl_retry: "{{ rclone_alerting_default_curl_retry }}"

### Telegram
rclone_alerting_telegram_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_telegram_curl_retry: "{{ rclone_alerting_default_curl_retry }}"
rclone_alerting_telegram_default_msg: "{{ rclone_alerting_default_subject }}"
rclone_alerting_telegram_emoji_fail: "{{ rclone_alerting_default_emoji_fail }}"
rclone_alerting_telegram_emoji_success: "{{ rclone_alerting_default_emoji_success }}"

rclone_alerting_telegram_default: false
rclone_alerting_telegram_default_api_key: ""
rclone_alerting_telegram_default_chat_id: ""

### Rocket.Chat
rclone_alerting_rocketchat_color_fail: "{{ rclone_alerting_default_color_fail }}"
rclone_alerting_rocketchat_color_success: "{{ rclone_alerting_default_color_success }}"
rclone_alerting_rocketchat_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_rocketchat_curl_retry: "{{ rclone_alerting_default_curl_retry }}"
rclone_alerting_rocketchat_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_rocketchat_default_title: "{{ rclone_alerting_default_subject }}"
rclone_alerting_rocketchat_emoji_fail: "{{ rclone_alerting_default_emote_fail }}"
rclone_alerting_rocketchat_emoji_success: "{{ rclone_alerting_default_emote_success }}"

rclone_alerting_rocketchat_default: false
rclone_alerting_rocketchat_default_webhook_url: ""

### Discord
rclone_alerting_discord_color_fail: "16068399"   # #f52f2f as an int
rclone_alerting_discord_color_success: "7454836"  # #71c074 as an int
rclone_alerting_discord_curl_max_time: "{{ rclone_alerting_default_curl_max_time }}"
rclone_alerting_discord_curl_retry: "{{ rclone_alerting_default_curl_retry }}"
rclone_alerting_discord_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_discord_default_title: "{{ rclone_alerting_default_subject }}"
rclone_alerting_discord_emoji_fail: "{{ rclone_alerting_default_emote_fail }}"
rclone_alerting_discord_emoji_success: "{{ rclone_alerting_default_emote_success }}"

rclone_alerting_discord_default: false
rclone_alerting_discord_default_webhook_url: ""

### Node Exporter - File Exporter
rclone_alerting_node_default: false
rclone_alerting_node_default_dir: "/var/lib/node-exporter/file_exporter/"

### Desktop notifications

#### Comma separated list of notification categories
rclone_alerting_libnotify_category: "rclone"
rclone_alerting_libnotify_default_body: "{{ rclone_alerting_default_body }}"
rclone_alerting_libnotify_default_title: "Rclone"
rclone_alerting_libnotify_emoji_fail: "{{ rclone_alerting_default_emoji_fail }}"
rclone_alerting_libnotify_emoji_success: "{{ rclone_alerting_default_emoji_success }}"
#### This must be a png
rclone_alerting_libnotify_icon_url: "https://github.com/rclone/rclone/raw/master/docs/static/img/rclone-32x32.png"
rclone_alerting_libnotify_timeout_fail: "30000"
rclone_alerting_libnotify_timeout_success: "10000"
rclone_alerting_libnotify_urgency_fail: "critical"
rclone_alerting_libnotify_urgency_success: "normal"

rclone_alerting_libnotify_default: false
rclone_alerting_libnotify_default_username: ""

## Retry
rclone_default_max_attempts: 3
rclone_default_retry_interval: 600

## Commands
## By default, use the functions defined in rclone_wrapper
rclone_default_cmd: "cri_sync"

## Logrotate
rclone_logrotate: true
rclone_log_retention_time: 12

## Extra vars
rclone_default_extra_vars_export: true

### Vars are being sourced in the retry_handler *and* the job script
### valid values:
###   {var: my_var, value: my_value, export: true|false(optionnal)}
rclone_default_extra_vars: []

## Cron
rclone_default_cron: true
rclone_default_cron_minute: '0'
rclone_default_cron_hour: '3'
rclone_default_cron_day: '*'
rclone_default_cron_weekday: '*'
rclone_default_cron_month: '*'

# Jobs definition
# List of dictionnaries defining the jobs
rclone_jobs_list: []

# This list will allow the user to deploy only a subset of the jobs
rclone_jobs_list_partial: []
