# Changelog

All notable changes to this project will be documented in this file.

## [unreleased] - since 0.1.0

## [0.1.0] - 2022-10-13

### Added

- build fileysstem tree;
- download rclone;
- configure jobs and special wrapper for `sync` ones.

[unreleased]: https://gitlab.cri.epita.fr/cri/iac/ansible/roles/rclone/-/compare/0.1.0...master
[0.1.0]: https://gitlab.cri.epita.fr/cri/iac/ansible/roles/rclone/-/releases/0.1.0
